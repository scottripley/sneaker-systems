# sneaker.systems

sneaker.systems - toy chat program
- [node.js](https://nodejs.org/)
- [express](http://expressjs.com/)
- [uwebsockets](https://www.npmjs.com/package/uws)

### Running Locally on Ubuntu 16:
```
sudo apt-get update
sudo apt-get install build-essential
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install npm
git clone https://gitlab.com/scottripley/sneaker-systems.git
cd sneaker-systems
sed -i  's|var host = "sneaker.systems";|var host = "<ip.address>";|' html/js/client.js
sed -i  's|var host = "sneaker.systems";|var host = "<ip.address>";|' html/js/client-mobile.js
if using SSL:
    place key & certificate files in ssl directory, for example:
    ssl/sneaker-systems.key  
    ssl/X509-certificate.pem
otherwise:
    sed -i  's|var ws_protocol = "wss";|var ws_protocol = "ws";|' html/js/client.js
    sed -i  's|var ws_protocol = "wss";|var ws_protocol = "ws";|' html/js/client-mobile.js
npm install
nodejs ./server.js >> ./logs/server.log 2>&1
```
your toy chat program is running on [http[s]://ip.address:8443](http[s]://ip.address:8443/).

### Running via Tor on Ubuntu 16:
```
sudo apt-get install tor
sudo mkdir /var/lib/tor/hidden_services/sneaker-systems
sudo chown debian-tor:debian-tor /var/lib/tor/hidden_services/sneaker-systems
sudo chmod 700 /var/lib/tor/hidden_services/sneaker-systems
echo "HiddenServiceDir /var/lib/tor/hidden_services/sneaker-systems" | sudo tee -a /etc/tor/torrc
echo "HiddenServicePort 8443 127.0.0.1:8443" | sudo tee -a /etc/tor/torrc
sudo service tor restart
hostname=`sudo cat /var/lib/tor/hidden_services/sneaker-systems/hostname`
cd sneaker-systems
sed -i  's|var host = \"sneaker.systems\";|var host = \"'"${hostname}"'\";|' html/js/client.js
sed -i  's|var ws_protocol = "wss";|var ws_protocol = "ws";|' html/js/client.js
sed -i  's|var protocol = "https";|var protocol = "http";|' html/js/client.js
sed -i  's|var host = \"sneaker.systems\";|var host = \"'"${hostname}"'\";|' html/js/client-mobile.js
sed -i  's|var ws_protocol = "wss";|var ws_protocol = "ws";|' html/js/client-mobile.js
sed -i  's|var protocol = "https";|var protocol = "http";|' html/js/client-mobile.js
rm ssl/*
nodejs ./server.js >> ./logs/server.log 2>&1
```
your toy chat program is running on [http://hostname.onion:8443](http://hostname.onion:8443/).

## Documentation

- [Configuring Hidden Services for Tor](https://www.torproject.org/docs/tor-hidden-service.html.en)
- [Tor Hidden (Onion) Services Best Practices](https://riseup.net/en/security/network-security/tor/onionservices-best-practices)
- ...
