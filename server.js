var fs = require('fs');
var http = require('http');
var https = require('https');
var ws = require('uws')
const util = require("util");

var debug = false;

var useSSL = false;  // this one for TOR hidden service!
//var useSSL = true;
var privateKeyFilename = "ssl/privkey.pem";
var certificateFilename = "ssl/fullchain.pem";

var privateKey;
var certificate;
var credentials;

if(useSSL && fs.existsSync(privateKeyFilename) && fs.existsSync(certificateFilename)) {
  useSSL = true;
  privateKey  = fs.readFileSync(privateKeyFilename, 'utf8');
  certificate = fs.readFileSync(certificateFilename, 'utf8');
  credentials = {key: privateKey, cert: certificate};
} else {
  useSSL = false;
}

var channels = {};
var connections = {};

var express = require('express');
var app = express();

var httpsServer;
var httpServer;
var wss;

var ipLocationsCached = {};
var ipLocationsApiCalls = 0;
var serverRemoteIP = "";

if(useSSL) {
  httpsServer = https.createServer(credentials, app);
  //httpsServer.listen(8443, "0.0.0.0");
  httpsServer.listen(8080, "0.0.0.0");
  wss = new ws.Server({server: httpsServer});
} else {
  httpServer = http.createServer(app);
  //httpServer.listen(8443, "0.0.0.0");
  httpServer.listen(8080, "0.0.0.0");
  wss = new ws.Server({server: httpServer});
}

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/clients', (req, res) => clients(req, res));
app.get('/status', (req, res) => status(req, res));
app.use('/', express.static('html'))

const getContent = function(url) {
  return new Promise((resolve, reject) => {
    const https = require('https')
    const request = https.get(url, (response) => {
      // handle http errors
      if (response.statusCode < 200 || response.statusCode > 299) {
        reject(new Error('Failed to load page, status code: ' + response.statusCode));
      }
      // temporary data holder
      const body = [];
      // on every content chunk, push it to the data array
      response.on('data', (chunk) => body.push(chunk));
      // we are done, resolve promise with those joined chunks
      response.on('end', () => resolve(body.join('')));
    });
    // handle connection errors of the request
    request.on('error', (err) => reject(err))
  })
}

async function fetchLocation(ipAddress) {
  if(ipAddress in ipLocationsCached) {
    //console.log("using cached location for: "+ipAddress);
    return ipLocationsCached[ipAddress];
  } else {
    //console.log("fetching location for: "+ipAddress+" (ipLocationsApiCalls: "+ipLocationsApiCalls+")");
    var ipLocationString = await getContent("https://api.ipgeolocation.io/ipgeo?apiKey=c3c719a495e04f64b9dc28be68960d01&ip="+ipAddress);
    var ipLocation = JSON.parse(ipLocationString);
    ipLocationsApiCalls = ipLocationsApiCalls+1;
    ipLocationsCached[ipAddress] = ipLocation;
    return ipLocationsCached[ipAddress];
  }
}


async function clients(req, res) {
  if(serverRemoteIP == "") {
    serverRemoteIP = await getContent("https://api.ipify.org/");
  }
  var ip_addresses = [];
  var ip_locations = [];
  var ipLocation = await fetchLocation(serverRemoteIP);
  ip_addresses.push(serverRemoteIP);
  ip_locations.push(ipLocation);
  for(var channel in channels) {
    var clients = channels[channel];
    for(var i = clients.length - 1; i >= 0; i--) {
      if(!(ip_addresses.includes(clients[i].remote_ip)) && (typeof(clients[i].remote_ip) != typeof(undefined))) {
        ip_addresses.push(clients[i].remote_ip)
        var ipLocation = await fetchLocation(clients[i].remote_ip);
        ip_locations.push(ipLocation);
      }
    }
  }
  res.send(JSON.stringify(ip_locations, null, 2));
}

function status(req, res) {
  var property = req.param("property");
  var channel = req.param("channel");
  if(property  == "channels") {
    res.send("{\"channelCount\":" + channelCount(channels) + "}");
  } else if(property  == "connections") {
    res.send("{\"connectionCount\":" + connectionCount() + "}");
  } else if(property  == "channelConnections") {
    res.send("{\"channelConnections\":" + connectionCount(channel) + ", \"activeChannels\":" + channelCount(channels) + ",\"activeConnections\":" + connectionCount() + "}");  
  } else {
    res.send("{\"channelCount\":" + channelCount(channels) + "}");
    res.send("{\"connectionCount\":" + connectionCount() + "}");
  };
};

wss.getUniqueID = function () {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  return s4() + s4() + '-' + s4();
};


wss.reaper = function reaper() {
  console.log("reaping connections...");
  var connCount = 0;
  for(var channel in channels) {
    var clients = channels[channel];
    for(var i = clients.length - 1; i >= 0; i--) {
      if(clients[i].readyState === ws.CLOSED) {
        if(debug) {
          console.log(timestamp() +": " + "wss.reaper() - connection: " + connections[clients[i].id] + " has disconnected.");
        }
        var system_msg = {
          handle: "SYSTEM",
          channel: channel,
          timestamp: timestamp(),
          text: connections[clients[i].id] + " disconnected."
        };
        wss.broadcast(channel, JSON.stringify(system_msg));
        delete connections[clients[i].id];
        clients.splice(i, 1);
      }
    }
    if(channels[channel].length === 0) {
      if(debug) {
        console.log(timestamp() +": " + "wss.reaper() - channel " + channel + " has 0 connections / deleting...");
      }
      delete(channels[channel]);
    } else {
      if(debug) {
        console.log(timestamp() +": " + "wss.reaper() - channel " + channel + " has " + channels[channel].length + " connection(s)");
      }
      connCount = connCount + channels[channel].length;
    }
  }
}

wss.broadcast = function broadcast(channel, data) {
  if(debug) {
    console.log(timestamp() +": " + "wss.broadcast(...) - channel: " + channel + " -> " + data);
  }
  if(channels[channel]) {
    channels[channel].forEach(function each(client) {
      if(client.readyState === ws.OPEN) {
        client.send(data);
      }
    });
  } else {
    if(debug) {
      console.log(timestamp() +": " + "wss.broadcast(...) - channel: " + channel + "... no clients?");
    }
  }
};

wss.on('connection', function(ws, req) {
  console.log("HEADERS:");
  console.log(JSON.stringify(req.headers));
  ws.id = wss.getUniqueID();
  var headers = req.headers;
  ws.remote_ip = req.headers['x-forwarded-for'];
  console.log("A) new connection: "+ws.remote_ip);
  console.log("B) new connection: "+req.remote_addr);
  console.log("C) new connection: "+util.inspect(req));
  wss.reaper() // required to delete disconnect first?
  var query = require('url').parse(ws.upgradeReq.url,true).query;
  var channel = require('url').parse(ws.upgradeReq.url,true).query.channel;
  var handle  = require('url').parse(ws.upgradeReq.url,true).query.handle;
  if(debug) {
    console.log(timestamp() +": " + "wss.broadcast(...) - new connection on channel: " + channel + " (" + handle + ")");
  }
  if(!channels[channel]) {
    channels[channel] = [];
  }
  channels[channel].push(ws); 
  connections[ws.id] = handle;
  if(debug) {
    console.log(ws.id + "->" + handle);
  }
  var system_msg = {
                     handle: "SYSTEM",
                     channel: channel,
                     timestamp: timestamp(),
                     text: handle + " is now connected on " + channel + "." 
                   };
  wss.broadcast(channel, JSON.stringify(system_msg));
  ws.on('message', function(msg) {
    try {
      var message = JSON.parse(msg);
      message["timestamp"] =  timestamp();
      wss.broadcast(message.channel, msg);
    } catch (e) {
      if(debug) {
        console.log("JSON error for message: " + msg);
      }
    }
  });
});

//wss.on('close', function() {
//});

setInterval(function(){
  wss.reaper();
}, 1000); 

function channelCount(channels) {
  return Object.keys(channels).length;
}

function connectionCount(channel) {
  if(channel) {
    if(channels[channel]) {
      return channels[channel].length;
    } else {
      return 0;
    }
  } else {
    var connectionCount = 0;
    for(var channel in channels) {
      connectionCount = connectionCount + channels[channel].length;
    }
    return connectionCount;
  }
}

function timestamp() {
  var date = new Date();
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  month = (month <= 9 ? '0' + month : month);
  var day = date.getDate();
  day = (day <= 9 ? '0' + day : day);
  var hour = date.getHours();
  hour = (hour <= 9 ? '0' + hour : hour);
  var minute = date.getMinutes();
  minute = (minute <= 9 ? '0' + minute : minute);
  var second = date.getSeconds();
  second = (second <= 9 ? '0' + second : second);
  return month + "/" + day + "/" + year + " " + hour + ":" + minute + ":" + second;
}

