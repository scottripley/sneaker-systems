$(document).ready(function(){

    var chatName = "", channel = "default";
    $("#chatNameButton").click(function(){
        channel = $("#channel").val();
        if(channel.length <= 0) channel = "default";
        $(location).attr('href',"#chatPage");
    });

    $("#text").bind( 'keydown', function(e) {
        if ((e.keyCode || e.charCode) !== 13) return true;
        $("#chatSendButton").click();
        return false;
    });

    $("#chatSendButton").click(function(){
        var handle = escapeHTML($('#handle').val());   
        var channel = escapeHTML($('#channel').val());   
        var text  = escapeHTML($('#messageText').val());   
        var message = {
            handle: handle,
            channel: channel,
            timestamp: timestamp(),
            text: text
    }

    var jsonMessage = JSON.stringify(message);
    websocket.send(jsonMessage);
        $('#messageText').focus();
        $('#messageText').val('');
    });

    var host = "sneaker.systems";
    var ws_protocol = "wss";
    var ws_port = 8443;
    var websocket = null;

    if(websocket) {  // disconnect if already connected...
        websocket.close();
    }

    var handle = $('#handle').val() || $_GET("handle") || localStorage.handle;
    handle = escapeHTML(handle);
    if(!handle) {
        handle = 'anon-' + guid();
    }
    localStorage.setItem("handle", handle) 
    $('#handle').val(handle);

    var channel = $('#channel').val() || $_GET("channel") || localStorage.channel || "default";
    channel = escapeHTML(channel);
    if(!channel) {
         channel = 'rnd-' + guid();
    }
    localStorage.setItem("channel", channel) 
    $('#channel').val(channel);

    websocket = new WebSocket(ws_protocol+"://"+host+":"+ws_port+"/?channel="+channel+"&handle="+handle);

    websocket.onmessage = function(event) {
        var message = JSON.parse(event.data);
        if(message.handle == "SYSTEM") {
            $("#incomingMessages").append("<div class='message'>" +
                                          message.text +
                                          "</div>");
        } else {
            $("#incomingMessages").append("<div class='message'><span class='username'>" +
                                          (message.handle) +
                                          "</span> : " +
                                          (message.text || ' ') +
                                          "</div>");
        }
        $("#incomingMessages").scrollTop($("#incomingMessages")[0].scrollHeight);
    };

    websocket.onerror = function(evt) {
        var message = null;
        if(websocket.readyState == WebSocket.CLOSED) { 
            message = "disconnected.";
        } else {
            message = "websocket readyState: " + websocket.readyState;
        }
        $("#incomingMessages").append("<div class='message'><span class='username'>" +
                                      "SYSTEM" +
                                      "</span> : " +
                                      message +
                                      "</div>");
        $("#incomingMessages").scrollTop($("#incomingMessages")[0].scrollHeight);
    };


});

