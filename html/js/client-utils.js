
// source: https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
function copyToClipboard(text) {
  if (window.clipboardData && window.clipboardData.setData) {
    // IE specific code path to prevent textarea being shown while dialog is visible.
    return clipboardData.setData("Text", text); 
  } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
    var textarea = document.createElement("textarea");
    textarea.textContent = text;
    textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
    document.body.appendChild(textarea);
    textarea.select();
    try {
      return document.execCommand("copy");  // Security exception may be thrown by some browsers.
    } catch (ex) {
      console.warn("Copy to clipboard failed.", ex);
      return false;
    } finally {
      document.body.removeChild(textarea);
    }
  }
}

function timestamp() {
  var date = new Date();
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  month = (month <= 9 ? '0' + month : month);
  var day = date.getDate();
  day = (day <= 9 ? '0' + day : day);
  var hour = date.getHours();
  hour = (hour <= 9 ? '0' + hour : hour);
  var minute = date.getMinutes();
  minute = (minute <= 9 ? '0' + minute : minute);
  var second = date.getSeconds();
  second = (second <= 9 ? '0' + second : second);
  return month + "/" + day + "/" + year + " " + hour + ":" + minute + ":" + second;
}

function guid() {
  return s4()+s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

// source: https://www.creativejuiz.fr/blog/en/javascript-en/read-url-get-parameters-with-javascript
function $_GET(param) {
  var vars = {};
  window.location.href.replace(location.hash, '').replace(/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
    function(m, key, value) {
      vars[key] = value !== undefined ? escapeHTML(value) : '';  // is this sufficient?
    }
  );
  if(param) {
    return vars[param] ? vars[param] : null;
  }
  return vars;
}

function filterString(str, regex, replacement) {
  if(str) {
    return str.replace(regex, replacement);
  } else {
    return str;
  }
}

// source: http://shebang.brandonmintern.com/foolproof-html-escaping-in-javascript/
// Use the browser's built-in functionality to quickly and safely escape
// the string
function escapeHTML(unsafe_str) {
  var safer_str = "";
  if(unsafe_str) {
    safer_str = $('<span>'+unsafe_str+'</span>').text();
  }
  return safer_str;
}
