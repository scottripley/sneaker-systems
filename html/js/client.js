
var protocol = "https";
var host = "sneaker.systems";
var ws_protocol = "wss";
var ws_port = 443;
var websocket = null;

if(window.location.hostname.includes("onion")) {
  protocol = "http";
  host = "uvnkvcceroypv53e.onion";
  ws_protocol = "ws";
  ws_port = 80;
  websocket = null;
}

function updateStatus() {
  var channel = $('#channel').val();
  channel = filterString(channel, /[^A-Za-z0-9+-]/g, "");
  $.ajax({
    //url: protocol+"://"+host+":"+ws_port+"/status",
    url: protocol+"://"+host+"/status",
    data: {
      property: "channelConnections",
      channel: channel
    },
    type: "GET",
    dataType: "json",
    success: function (data) {
      var channelConnections = escapeHTML(data.channelConnections);
      var activeChannels = escapeHTML(data.activeChannels);
      var activeConnections = escapeHTML(data.activeConnections);
      $("#channelConnections").html(channelConnections + " connections.");
      $("#status").html("(" + activeChannels + " active channels / " + activeConnections + " connections)");
    },
    error: function (xhr, status) {
      $("#channelConnections").html(status);
    },
    complete: function (xhr, status) {
      //$('#showresults').slideDown('slow')
    }
  });
}

var timerID = 0; 
function keepAlive() { 
  var timeout = 20000;  
  if (websocket.readyState == websocket.OPEN) {  
    websocket.send('');  
  }  
  timerId = setTimeout(keepAlive, timeout);  
}
 
function cancelKeepAlive() {  
  if (timerId) {  
    clearTimeout(timerId);  
  }  
}

function fetchIPLocation(ip_address) {
  var coords = [];
  jQuery.ajax({
    url: "https://api.ipgeolocation.io/ipgeo?apiKey=<hidden>&ip="+ip_address,
    success: function(data) {
      coords.push([data.latitude, data.longitude]);
    },
    async:false
  });
  return coords;
}

function fetchClientIPs() {
  //window.clients = window.clients.splice(0,window.clients.length);
  window.clients = [];
  jQuery.ajax({
    url: window.protocol+"://"+window.host+"/clients",
    success: function(ips) {
      ips.forEach(function(ip){
        window.clients.push(ip);
      });
     plotClientIPs();
    },
    async:false,
    dataType: "json"
  });
}

function plotClientIPs() {
  dataSource.clear();
  var f1 = new ol.Feature();
  f1.setGeometryName(JSON.stringify(window.clients[0], null, 2));
  var server_coord = [Number(window.clients[0].longitude), Number(window.clients[0].latitude)];
  f1.setGeometry(new ol.geom.Point(server_coord));
  f1.setStyle(redCircle);
  dataSource.addFeature(f1);
  if(window.clients.length > 1) {
    for(i=1; i < window.clients.length; i=i+1) {
      var client_coord = [Number(window.clients[i].longitude), Number(window.clients[i].latitude)];
      var arcGenerator = new arc.GreatCircle(
        {x: server_coord[0], y: server_coord[1]},
        {x: client_coord[0], y: client_coord[1]});
      var coords = [];
      var arcLine = arcGenerator.Arc(100, {offset: 10});
      arcLine.geometries.forEach(function(geom) { coords.push(geom.coords); });
      var multiLineString = new ol.geom.MultiLineString(coords);
      var f1 = new ol.Feature(multiLineString);
      f1.setStyle(redArc);
      //f1.setGeometryName(JSON.stringify(window.clients[i], null, 2));
      dataSource.addFeature(f1);
      var f2 = new ol.Feature();
      f2.setGeometryName(JSON.stringify(window.clients[i], null, 2));
      f2.setGeometry(new ol.geom.Point(client_coord));
      f2.setStyle(redCircle);
      dataSource.addFeature(f2);
    };
  }
}

function check_connection() {
  console.log("checking connection...");
  if(websocket.readyState == WebSocket.CLOSED) {
    console.log("lost connection / attempting to reconnect...");
    init();
  }
  fetchClientIPs();
  updateStatus();
}


function init() {

  localStorage.clear()

  if(websocket) {  // disconnect if already connected...
    websocket.close();
  }

  // fetch or set handle...
  var handle = $('#handle').val() || $_GET("handle") || localStorage.handle;
  handle = filterString(handle, /[^A-Za-z0-9+-]/g, "");
  if(!handle) {
    handle = 'anon-' + guid();
  }
  localStorage.setItem("handle", handle) 
  $('#handle').val(handle);

  // fetch or set channel...
  var channel = $('#channel').val() || $_GET("channel") || localStorage.channel || "default";
  channel = filterString(channel, /[^A-Za-z0-9+-]/g, "");
  if(!channel) {
    channel = 'rnd-' + guid();
  }
  localStorage.setItem("channel", channel) 
  $('#channel').val(channel);

  //websocket = new WebSocket(ws_protocol+"://"+host+":"+ws_port+"/?channel="+channel+"&handle="+handle);
  websocket = new WebSocket(ws_protocol+"://"+host+"/?channel="+channel+"&handle="+handle);
  keepAlive();

  //websocket.onclose = function(event) {
  //};

  //websocket.onopen = function(event) {
  //};

  websocket.onmessage = function(event) {
    var message = JSON.parse(event.data);
    text = escapeHTML(message.text);
    text = text.replace(/(?:\r\n|\r|\n)/g, '<br />');
    var handle = escapeHTML(message.handle);
    if(handle == "SYSTEM") {
      $("#messages").append("<div class=\"message\" style=\"padding: 5px 5px 5px 5px;border-bottom: 1px solid #00FF00;\">" +
      (text || '') +
      " (" + timestamp() + ")" +
      "</div>");
    } else {
      $("#messages").append("<div class=\"message\" style=\"padding: 5px 5px 5px 5px;border-bottom: 1px solid #00FF00;\">" +
      "<span style='font-weight: bold'>" +
      (handle || 'Anonymous') + ": " +
      "</span>" +
      " (" + timestamp() + ")<br/>" +
      (text || ' ') +
      "</div>");
    }
    $("#messages").scrollTop($("#messages")[0].scrollHeight);
  };

  websocket.onerror = function(evt) {
    var message = null;
    if(websocket.readyState == WebSocket.CLOSED) { 
      message = "disconnected.";
    } else {
      message = "websocket readyState: " + websocket.readyState;
    }
    $("#messages").append("<div class=\"message\" style=\"padding: 5px 5px 5px 5px;border-bottom: 1px solid #00FF00;\">" +
      (message) +
      " (" + timestamp() + ")" +
      "</div>");
      $("#messages").scrollTop($("#messages")[0].scrollHeight);
  };

}

var redArc = new ol.style.Style({
  stroke: new ol.style.Stroke({
    width: 1,
    color: 'red'
  })
});

var redStroke = new ol.style.Stroke({
  color: '#FF0000',
  width: 1.0
});

var redFill = new ol.style.Fill({
  color: 'rgba(255,0,0,0.8)'
});

var redCircle = new ol.style.Style({
  image: new ol.style.Circle({
    fill: redFill,
    stroke: redStroke,
    radius:3
  }),
  fill: redFill,
  stroke: redStroke
});


var dataSource = new ol.source.Vector();

var dataLayer = new ol.layer.Vector({
  source: dataSource,
});

var dataStyle = new ol.style.Style({
  image: new ol.style.Icon({
    anchor: [0.5, 46],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    src: 'http://openlayers.org/en/latest/examples/data/icon.png'
  })
});

var clients = [];


$(document).ready(function() {

  init();

  //$('#console #text').keydown(function(event) {
  //  if (event.keyCode == 13 && event.shiftKey) {
  //    $('#console').submit();
  //    return false;
  //  }
  //});

  $('form').submit(function() {
    var handle = $('#handle').val();   
    handle = filterString(handle, /[^A-Za-z0-9]+-/g, "");
    var channel = $('#channel').val();   
    channel = filterString(channel, /[^A-Za-z0-9]+-/g, "");
    var text  = escapeHTML($('#text').val());   
    var message = {
      handle: handle,
      channel: channel,
      timestamp: timestamp(),
      text: text
    }
    var jsonMessage = JSON.stringify(message);
    websocket.send(jsonMessage);
      $('#text').focus();
      $('#text').val('');
      return false;
  });

  var checkTimer = setInterval(check_connection, 3000);

  var world = new ol.layer.Vector({
    source: new ol.source.Vector({
    //projection: 'EPSG:3857',
    projection: 'EPSG:4326',
    format: new ol.format.GeoJSON(),
      url: 'openlayers/data/ne_110m_admin_0_countries_lakes.geojson'
    }),
    style: function(feature, resolution) {
      var text = resolution < 5000 ? feature.get('name') : '';
      var style = [
        new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(0, 0, 0, 0.6)'
          }),
          stroke: new ol.style.Stroke({
            color: '#00FF00',
            width: 1
          }),
          text: new ol.style.Text({
            font: '12px Calibri,sans-serif',
            text: text,
            fill: new ol.style.Fill({
              color: '#000'
            }),
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 3
            })
          })
        })
      ];
      return style;
    }
  });

  var dataStyle = new ol.style.Style({
    image: new ol.style.Icon({
      anchor: [0.5, 46],
      anchorXUnits: 'fraction',
      anchorYUnits: 'pixels',
      src: 'http://openlayers.org/en/latest/examples/data/icon.png'
    })
  });

  // setup map:
  var center = ol.proj.transform([0,0], 'EPSG:4326', 'EPSG:3857');

  var view = new ol.View({
    projection: 'EPSG:4326',
    //projection: 'EPSG:3857',
    center: center,
    zoom:1
  });

  var map = new ol.Map({
    controls: ol.control.defaults().extend([
      new ol.control.FullScreen()
    ]),
    layers: [
      world,
      dataLayer,
    ],
    target: 'map',
    view: view
  });

  var tooltip = document.getElementById('tooltip');
  var overlay = new ol.Overlay({
    element: tooltip,
    offset: [10, 0],
    positioning: 'bottom-left'
  });
  map.addOverlay(overlay);

  function displayTooltip(evt) {
    var pixel = evt.pixel;
    var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
      return feature;
    });
    tooltip.style.display = feature ? '' : 'none';
    //if(typeof(feature.get("name")) != typeof(undefined)) {
    if(feature && feature.getGeometryName() != "geometry") {
      overlay.setPosition(evt.coordinate);
      //tooltip.innerHTML = "<pre>"+feature.getGeometryName()+"</pre>";
      var locationInfo = JSON.parse(feature.getGeometryName());
      tooltip.innerHTML = locationInfo.ip + " / " + locationInfo.isp + " / " + locationInfo.country_name;
      //alert(feature.getGeometryName());
    }
  };

  map.on('click', displayTooltip);

  fetchClientIPs();
  updateStatus();

});


